<!-- Please include a one sentence summary of the enhancement as the issue title. Just typing "I have an idea" or "make [this] better" is not helpful! -->

<!--
    Questions to consider:
    - How will this improve cartogrtist?
    - How is this in cartogrtist's scope?
    - Does implmenting it require additional libraries?
    - How will it be worth the effort?
-->
