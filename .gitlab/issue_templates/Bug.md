<!-- Please include a one sentence summary of the bug as the issue title. Just typing "help" or "[this] doesn't work" is not helpful! -->

<!-- Remember: NOBODY CAN READ YOUR MIND OR SEE YOUR SCREEN WHEN YOU POST AN ISSUE! Please put all information you think is relevant in *this initial post*. It's better to provide too much information than not enough information. -->

### Checklist
* [ ] I'm on the latest version/commit of cartogrtist
* [ ] I can reproduce the bug every time I want it to happen
* I have searched both open and closed issues, and:
    * [ ] mine hasn't been submitted before
    * [ ] mine is an issue that *was* fixed, but has come up again (regression)
    * [ ] mine is simular to (an) issue(s) that has/have been submitted before, but I think this one is different <!-- Please explain if so. -->

### Summary
<!-- Type a slightly longer summary. -->

### Input
<!-- Provide the files and command you used. -->

### Expected Behavior
<!-- What do you expect cartogrtist to do? -->

### Actual Behavior
<!-- What did cartogrtist actually do? Include any console output (please add the -C flag to your command) and any other files generated. -->

### Proposed Solution
<!-- If you know, how do you think this bug can be fixed? -->

### System Info
<!-- Put your OS and cartogrtist version here. -->
