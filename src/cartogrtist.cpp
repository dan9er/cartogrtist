/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <fstream>
#include <stdexcept>
#include <chrono>

#include <Magick++.h>
#include <tclap/ArgException.h>

#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_sinks.h>

#ifdef _WIN32
    #include <io.h>
    #include <fcntl.h>
#endif

#include "cliParser.h"
#include "mapArtFlat.h"
#include "mapArtStaircase.h"
#include "palette.h"

extern template class schematic<128,  2,129>;
extern template class schematic<128,130,129>;

void      flatLogic(const Magick::Image&, const cliParser&);
void staircaseLogic(const Magick::Image&, const cliParser&);
void        initLog();

//! Main method.
int main(int argc, char const* argv[])
{
    try
    {
        // make note of the time (to print time elapsed later)
        std::chrono::steady_clock::time_point startTime(std::chrono::steady_clock::now());

        // init logger
        initLog();

        // if on windows, force stdin/out to binary mode
        #ifdef _WIN32
            setmode(fileno(stdout),O_BINARY);
            setmode(fileno(stdin),O_BINARY);
        #endif

        // parse flags
        cliParser cli(argc, argv);

        // init Magick
        Magick::InitializeMagick(*argv);

        // get our input image from file
        spdlog::info("- Reading image: {}", cli.getInputPath());
        Magick::Image inputImage;
        try
            {inputImage.read(cli.getInputPath());}
        catch (Magick::Warning& w)
            {spdlog::warn("Image processing warning: {}", w.what());}
        catch (Magick::ErrorFileOpen& e)
        {
            spdlog::critical("Can't open '{}', make sure it exists & you have permission to read it",
                cli.getInputPath());
            return 1;
        }

        // check size of image
        if((inputImage.columns() != 128) || inputImage.rows() != 128)
        {
            spdlog::critical("The input image must have a size of 128x128, '{}' is {}x{}",
                cli.getInputPath(), inputImage.columns(), inputImage.rows());
            return 1;
        }

        //! \todo quantize and dither the image

        // convert the image into a schematic and write it to file
        switch (cli.getMethod())
        {
            case palette::methodType::flat:
                spdlog::debug("Using flat method");
                flatLogic(inputImage, cli);
            break;

            case palette::methodType::staircase:
                spdlog::debug("Using staircase method");
                staircaseLogic(inputImage, cli);
            break;

            default:
                throw std::runtime_error("CLI returned unexpected methodType");
        }

        // done!
        spdlog::info
        (
            "Done ({} s)",
            (double)(
                std::chrono::duration_cast<std::chrono::milliseconds>
                (
                    std::chrono::steady_clock::now() - std::move(startTime)
                ).count()
            ) / 1000
        );
        return 0;
    }
    catch (const TCLAP::ArgException& e)
    {
        spdlog::critical("Error reading flags: {} for arg {}", e.error(), e.argId());
        return 1;
    }
    catch (const Magick::Error& e)
    {
        spdlog::critical("Image processing error: {}", e.what());
        return 1;
    }
    catch (const spdlog::spdlog_ex& e)
    {
        spdlog::critical("Logger error: {}", e.what());
        return 1;
    }
    catch (const std::exception& e)
    {
        spdlog::critical("Error: {}", e.what());
        return 1;
    }
    catch (...)
    {
        spdlog::critical("Unknown error");
        return 1;
    }
}

//! The logic for the flat method.
/*! \param inputImage The Magick::Image to process.
    \param cli The cliParser to grab values from.
    \see mapArtFlat palette::methodType::flat */
void flatLogic(const Magick::Image& inputImage, const cliParser& cli)
{
    spdlog::trace("flatLogic(const Magick::Image&, const cliParser&)");

    spdlog::info("- Converting to internal mapart format");
    mapArtFlat art(inputImage, cli.getPalette());

    spdlog::info("- Converting to internal schematic format");
    schematic<128,2,129> schem(std::move(art.exportSchematic()));

    spdlog::info("- Writing schematic: {}", cli.getOutputPath());
    if (cli.getOutputPath() == "-")
        std::cout << std::move(schem) << std::flush;
    else
        std::ofstream(cli.getOutputPath(), std::ios::out|std::ios::binary) << std::move(schem) << std::flush;
}

//! The logic for the staircase method.
/*! \param inputImage The Magick::Image to process.
    \param cli The cliParser to grab values from.
    \see mapArtStaircase palette::methodType::staircase */
void staircaseLogic(const Magick::Image& inputImage, const cliParser& cli)
{
    spdlog::trace("staircaseLogic(const Magick::Image&, const cliParser&)");

    spdlog::info("- Converting to internal mapart format");
    mapArtStaircase art(inputImage, cli.getPalette());

    spdlog::info("- Converting to internal schematic format");
    schematic<128,130,129> schem(std::move(art.exportSchematic()));

    spdlog::info("- Writing schematic: {}", cli.getOutputPath());
    if (cli.getOutputPath() == "-")
        std::cout << std::move(schem) << std::flush;
    else
        std::ofstream(cli.getOutputPath(), std::ios::out|std::ios::binary) << std::move(schem) << std::flush;
}

//! Sets up \c spdlog.
void initLog()
{
    // set default logger
    spdlog::set_default_logger
    (
        std::make_shared<spdlog::logger>
        (
            "colorlessLogger",
            std::make_shared<spdlog::sinks::stderr_sink_mt>()
        )
    );

    // set default logger pattern
    spdlog::set_pattern("[%8l] %v");
}
