/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "staircaseColumn.h"

#include <array>
#include <algorithm>
#include <utility>
#include <memory>

#include <Magick++.h>

#include <spdlog/spdlog.h>

#include "palette.h"

//--------------//
// CTORS & DTOR //
//--------------//

staircaseColumn::staircaseColumn() :
    blocks(new std::array<block,129>()),
    height(new std::array<  int,129>())
{
    spdlog::trace("staircaseColumn::staircaseColumn()");
    this->height->fill(0);
}

staircaseColumn::staircaseColumn(const staircaseColumn& c) :
    blocks(new std::array<block,129>(*(c.blocks))),
    height(new std::array<  int,129>(*(c.height)))
{
    spdlog::trace("staircaseColumn::staircaseColumn(const staircaseColumn&)");}

staircaseColumn& staircaseColumn::operator=(const staircaseColumn& c)
{
    spdlog::trace("staircaseColumn::opertor=(const staircaseColumn&)");

    // don't do anything if trying to copy to self
    if (this->blocks != c.blocks)
        this->blocks.reset(new std::array<block,129>(*(c.blocks)));
    if (this->height != c.height)
        this->height.reset(new std::array<  int,129>(*(c.height)));

    return *this;
}

staircaseColumn::staircaseColumn(staircaseColumn&& c) :
    blocks(c.blocks.release()),
    height(c.height.release())
{
    spdlog::trace("staircaseColumn::staircaseColumn(staircaseColumn&&)");}

staircaseColumn& staircaseColumn::operator=(staircaseColumn&& c)
{
    spdlog::trace("staircaseColumn::operator=(const staircaseColumn&)");

    // don't do anything if trying to move to self
    if (this->blocks != c.blocks)
        this->blocks.reset(c.blocks.release());
    if (this->height != c.height)
        this->height.reset(c.height.release());

    return *this;
}

staircaseColumn::staircaseColumn(const Magick::Image& i, const palette& p) :
    blocks(new std::array<block,129>()),
    height(new std::array<  int,129>())
{
    spdlog::trace("staircaseColumn::staircaseColumn(const Magick::Image&, const palette&)");

    this->blocks->at(128) = block(3,0, "minecraft:dirt", Magick::ColorRGB("#000000"));

    // for every row (pixel) in i...
    for (std::size_t y = 0; y != 128; ++y)
    {
        spdlog::debug("y: {}", y);

        // grab the pixel's color from i
        Magick::ColorRGB pixel(i.pixelColor(0, 127-y));

        // look up that color in the palette for it's block
        block b(p.find(pixel, palette::methodType::staircase));

        // add that block to this->blocks
        this->blocks->at(y) = b;
        spdlog::debug("block used: {}", b);

        // calculate the next height value
        int heightNo = this->height->at(y);
        switch (b.getColorType(pixel))
        {
            case block::colorType::low:
                ++heightNo;
            break;

            case block::colorType::base:
            break;

            case block::colorType::high:
                --heightNo;
            break;
        }

        // add it to this->height
        spdlog::trace("heightNo = {}", heightNo);
        this->height->at(y+1) = std::move(heightNo);
    }

    // normalize the heights
    this->normalizeHeight();
}

/*virtual*/ staircaseColumn::~staircaseColumn()
    {spdlog::trace("staircaseColumn::~staircaseColumn()");}

//----------------//
// PUBLIC MEMBERS //
//----------------//

std::pair<block&, int&> staircaseColumn::at(std::size_t n)
{
    spdlog::trace("staircaseColumn::at(std::size_t = {})", n);
    return {this->blocks->at(n), this->height->at(n)};
}
std::pair<const block&, const int&> staircaseColumn::at(std::size_t n) const
{
    spdlog::trace("staircaseColumn::at(std::size_t = {}) const", n);
    return {this->blocks->at(n), this->height->at(n)};
}

staircaseColumn& staircaseColumn::normalizeHeight()
{
    spdlog::trace("staircaseColumn::normalizeHeight()");

    int offset = *(std::min_element(this->height->cbegin(), this->height->cend()));

    for (int& i : *(this->height))
        i -= offset;

    return *this;
}
