/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <string>
#include <stdint.h>

#include "palette.h"

//! Manages and incapsulates CLI processing.
class cliParser
{
    public:

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! \warning No default constructor.
        cliParser()
            = delete;

        //! \warning No copy constructor.
        cliParser(const cliParser&)
            = delete;

        // No copy operator.

        //! Move constructor.
        /*! \param c cliParser to move from. */
        cliParser(cliParser&&);

        //! Move operator.
        /*! \param c cliParser to move from.
            \returns A reference to the cliParser moved to. */
        cliParser& operator=(cliParser&&);

        //! Constructor accepting \c argc & \c argv variables from main().
        /*! \param argc \c argc from main().
            \param argv \c argv from main(). */
        cliParser(int, char const* []);

        //! Destructor.
        virtual ~cliParser();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Returns a \c const reference to \ref inputPath.
        const std::string& getInputPath() const;

        //! Returns a \c const reference to \ref outputPath.
        const std::string& getOutputPath() const;

        //! Returns a \c const reference to \ref pal.
        const palette& getPalette() const;

        //! Returns a \c const reference to \ref method.
        const palette::methodType& getMethod() const;

    private:

        //------//
        // DATA //
        //------//

        //! The file path to the input image.
        std::string inputPath;

        //! The file path to output to.
        std::string outputPath;

        //! The palette to use.
        palette pal;

        //! The map-making method to use.
        palette::methodType method;
};
