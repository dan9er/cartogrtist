/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <string>
#include <array>
#include <vector>

#include <Magick++.h>

#include <tclap/CmdLine.h>

// check if spdlog has internal fmt
#include <spdlog/tweakme.h>
#if !defined(SPDLOG_FMT_EXTERNAL)
    #include <spdlog/fmt/bundled/format.h>
#else
    #include <fmt/format.h>
#endif

#include "block.h"
#include "staircaseColumn.h"
#include "schematic.h"

/*! \file
    \brief Holds most of the template instantiation definitions used in the program.

    This setup is so compiled objects that share instantiations don't have to each store their own copy. */

template class std::array<int,129>;
template class std::array<block,129>;
template class std::array<std::array<block,129>,128>;
template class std::array<staircaseColumn,128>;

template class std::vector<std::string>;

template class std::set<block>;
template class std::initializer_list<block>;

template class TCLAP::UnlabeledValueArg<std::string>;
template class TCLAP::ValuesConstraint<std::string>;
template class TCLAP::ValueArg<std::string>;

template class schematic<128,  2,129>;
template class schematic<128,130,129>;
