/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <string>
#include <set>
#include <initializer_list>
#include <stdint.h>

#include <Magick++.h>

#include "block.h"

extern template class std::set<block>;
extern template class std::initializer_list<block>;

//! Represents a group of \ref block s and provides lookup member functions.
class palette
{
    public:

        //-------//
        // ENUMS //
        //-------//

        //! Specifies which built-in \ref palette to use.
        /*! \see palette(const palette::builtin&) */
        enum class builtin : uint8_t
        {
            one_twelve //!< paletteBuiltinData::one_twelve
        };

        //! Specifies which mapart making method to use.
        enum class methodType : uint8_t
        {
            flat,      //!< Simpily lay the blocks flat.
            staircase, //!< Changes the height of the blocks to get triple the available colors.
            mapdat     //!< Write a map \c .dat file to inject directly into a world folder. Has 4x the available colors by using a 4th unused color.
                      /*! \warning \e UNIMPLMENTED. */
        };

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! Default constructor.
        /*! Initializes \ref blocks as an empty std::set. */
        palette();

        //! Copy constructor.
        /*! \param p \ref palette to copy from. */
        palette(const palette&);

        //! Copy operator.
        /*! \param p \ref palette to copy from.
            \returns A reference to the \ref palette copied to. */
        palette& operator=(const palette&);

        //! Move constructor.
        /*! \param p \ref palette to move from. */
        palette(palette&&);

        //! Move operator.
        /*! \param p \ref palette to move from.
            \returns A reference to the \ref palette moved to. */
        palette& operator=(palette&&);

        //! Constructor accepting a builtin palette.
        /*! \param v The builtin palette to use.*/
        palette(const builtin&);

        //! Constructor accepting a std::initializer_list.
        /*! \param l The std::initializer_list of blocks to initialize \ref blocks to. */
        palette(std::initializer_list<block>&&);

        // TODO implment reading palettes from file
        //palette(std::istream&);

        //! Destructor.
        virtual ~palette();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Adds a \ref block to \ref blocks.
        /*! \param b The \ref block to add.
            \returns A reference to the \ref palette called. */
        palette& insert(const block&);

        //! Removes a \ref block from \ref blocks.
        /*! \param b The \ref block to remove.
            \returns A reference to the \ref palette called.
            \throws std::invalid_argument if \p b doesn't exist in \ref blocks . */
        palette& erase(const block&);

        //! Lookup a \ref block via numerical IDs.
        /*! \param bi The block::bId of the \ref block to find.
            \param di The block::dId of the \ref block to find.
            \returns A constant reference to the \ref block that matches \p bi and \p di.
            \throws std::runtime_error if a \ref block in \ref blocks that matches \p bi and \p di can't be found.
            \see find(const std::string&) const
            \see find(const Magick::ColorRGB&,const methodType&) const */
        const block& find(const uint8_t&, const uint8_t& = 0) const;

        //! Lookup a \ref block via namespaced ID.
        /*! \param ni The block::nsId of the \ref block to find.
            \returns A constant reference to the \ref block that matches \p ni.
            \throws std::runtime_error if a \ref block in \ref blocks that matches \p ni can't be found.
            \see find(const uint8_t&,const uint8_t&) const
            \see find(const Magick::ColorRGB&,const methodType&) const */
        const block& find(const std::string&) const;

        //! Lookup a \ref block via Magick::ColorRGB.
        /*! \param c One of the Magick::ColorRGBs of the \ref block to find.
            \param m The palette::methodType for the lookup. This is to prevent wasteful comparisons if you don't need them.
            \returns A constant reference to the \ref block that matches \p c.
            \throws std::runtime_error if a \ref block in \ref blocks that matches \p c can't be found.
            \see find(const uint8_t&,const uint8_t&) const
            \see find(const std::string&) const */
        const block& find(const Magick::ColorRGB&, const methodType&) const;

        //! Generate a image to pass to Magick++'s quantize function.
        /*! \param m The palette::methodType to generate the Magick::Image for.
            \returns A Magick::Image that contains all the colors from the \ref block s in \ref blocks. */
        Magick::Image generateImage(const methodType&) const;

    private:

        //------//
        // DATA //
        //------//

        //! Contains all the \ref block s in the \ref palette.
        std::set<block> blocks;
};
