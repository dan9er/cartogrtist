/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "palette.h"

#include <string>
#include <set>
#include <algorithm>
#include <stdexcept>
#include <stdint.h>
#include <utility>

#include <Magick++.h>

#include <spdlog/spdlog.h>

#include "block.h"
#include "paletteBuiltinData.h"

extern template class std::initializer_list<block>;

//--------------//
// CTORS & DTOR //
//--------------//

palette::palette() :
    blocks()
{
    spdlog::trace("palette::palette()");}

palette::palette(const palette& p) :
    blocks(p.blocks)
{
    spdlog::trace("palette::palette(const palette&)");}

palette& palette::operator=(const palette& p)
{
    spdlog::trace("palette::operator=(const palette&)");

    this->blocks = p.blocks;

    return *this;
}

palette::palette(palette&& p) :
    blocks(std::move(p.blocks))
{
    spdlog::trace("palette::palette(palette&&)");}

palette& palette::operator=(palette&& p)
{
    spdlog::trace("palette::operator=(palette&&)");

    this->blocks = std::move(p.blocks);

    return *this;
}

palette::palette(const palette::builtin& v)
{
    spdlog::trace("palette::palette(const builtin& = {})", v);

    switch (v)
    {
        case palette::builtin::one_twelve:
            spdlog::debug("Using built-in _1.12 palette");
            this->blocks = paletteBuiltinData::one_twelve;
        break;
    }
}

palette::palette(std::initializer_list<block>&& l) :
    blocks(std::move(l))
{
    spdlog::trace("palette::palette(std::initializer_list<block>&& )");}

/*virtual*/ palette::~palette()
    {spdlog::trace("palette::~palette()");}

//----------------//
// PUBLIC MEMBERS //
//----------------//

palette& palette::insert(const block& b)
{
    spdlog::trace("palette::insert(const block& = {})", b);

    this->blocks.insert(b);
    return *this;
}

palette& palette::erase(const block& b)
{
    spdlog::trace("palette::erase(const block& = {})", b);

    // find the block in the array
    if (this->blocks.find(b) != this->blocks.cend())
    {
        this->blocks.erase(b);
        return *this;
    }
    else // block doesn't exist
        {throw std::invalid_argument("block doesn't exist");}
}

const block& palette::find(const uint8_t& bi, const uint8_t& di /*= 0*/) const
{
    spdlog::trace("palette::find(const uint8_t& bi = {}, di = {}) const", bi, di);

    // find the block in the array
    auto iter = std::find_if(this->blocks.cbegin(), this->blocks.cend(),
        [&bi, &di] (const block& i)
        {
            return    i.blockId() == bi
                   && i.dataId()  == di;
        }
    );

    if (iter != this->blocks.cend())
        {return *iter;}
    else // block doesn't exist
        {throw std::runtime_error("block not found");}
}

const block& palette::find(const std::string& ni) const
{
    spdlog::trace("palette::find(const std::string& = {}) const", ni);

    // find the block in the array
    auto iter = std::find_if(this->blocks.cbegin(), this->blocks.cend(),
        [&ni] (const block& i)
            {return i.namespacedId() == ni;}
    );

    if (iter != this->blocks.cend())
        {return *iter;}
    else // block doesn't exist
        {throw std::runtime_error("block not found");}
}

const block& palette::find(const Magick::ColorRGB& c, const palette::methodType& m) const
{
    spdlog::trace("palette::find(const Magick::ColorRGB& = {}, const methodType& = {}) const", c, m);

    // store the bool function that will be used in std::find_if
    std::function<bool(const block&)> testFunc;
    switch (m)
    {
        case palette::methodType::flat:
            testFunc = [&c] (const block& i)
                {return i.color() == c;};
        break;

        case palette::methodType::staircase:
            testFunc = [&c] (const block& i)
            {
                return    i.color(block::colorType::low)  == c
                       || i.color(block::colorType::base) == c
                       || i.color(block::colorType::high) == c;
            };
        break;
    }

    // find the block in the array
    auto iter = std::find_if(this->blocks.cbegin(), this->blocks.cend(), std::move(testFunc));

    if (iter != this->blocks.cend())
        {return *iter;}
    else // block doesn't exist
        {throw std::runtime_error(fmt::format("block not found for color {}", c));}
}

Magick::Image palette::generateImage(const palette::methodType& m) const
{
    spdlog::trace("palette::generateImage(const methodType& = {}) const", m);

    // figure out how many colors there are in total
    std::set<block>::size_type j = this->blocks.size();
    switch (m)
    {
        case palette::methodType::staircase:
            j *= 3; // 3 colors per block
        break;

        case palette::methodType::mapdat:
            j *= 4; // 4 colors per block
        break;

        default:
            // no nothing, 1 color per block
        break;
    }

    // make a blank image large enough to contain all the colors (1 pixel per color)
    Magick::Image image
    (
        Magick::Geometry
            (j, 1),
        "white"
    );

    // for every block in the palette...
    j = 0; // reuse j
    switch (m)
    {
        case palette::methodType::flat:
            for (const block& i : this->blocks)
                image.pixelColor(j++, 0, i.color());  // paint a pixel with it's color
        break;

        case palette::methodType::staircase:
            for (const block& i : this->blocks)
            {
                // paint pixels with it's colors
                image.pixelColor( j   * 3     , 0, i.color(block::colorType::low));
                image.pixelColor((j   * 3) + 1, 0, i.color(block::colorType::base));
                image.pixelColor((j++ * 3) + 2, 0, i.color(block::colorType::high));
            }
        break;

        // TODO implment palette::methodType::mapdat
    }

    // done, return the image
    return image;
}
