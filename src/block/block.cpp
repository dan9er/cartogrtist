/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "block.h"

#include <iostream>
#include <string>
#include <stdexcept>
#include <stdint.h>
#include <utility>

#include <Magick++.h>

#include <spdlog/spdlog.h>

// check if spdlog has internal fmt
#include <spdlog/tweakme.h>
#if !defined(SPDLOG_FMT_EXTERNAL)
    #include <spdlog/fmt/bundled/format.h>
#else
    #include <fmt/format.h>
#endif

//--------------//
// CTORS & DTOR //
//--------------//

block::block() :
    bId      (0),
    dId      (0),
    nsId     ("minecraft:air"),
    lowColor (Magick::ColorRGB("#000000")),
    baseColor(Magick::ColorRGB("#000000")),
    highColor(Magick::ColorRGB("#000000")),
    supp     (false)
{
    spdlog::trace("block::block()");}

block::block(const block& b) :
    bId      (b.bId),
    dId      (b.dId),
    nsId     (b.nsId),
    lowColor (b.lowColor),
    baseColor(b.baseColor),
    highColor(b.highColor),
    supp     (b.supp)
{
    spdlog::trace("block::block(const block& = {})", b);}

block& block::operator=(const block& b)
{
    spdlog::trace("block::operator=(const block& = {})", b);

    this->bId       = b.bId;
    this->dId       = b.dId;
    this->nsId      = b.nsId;
    this->lowColor  = b.lowColor;
    this->baseColor = b.baseColor;
    this->highColor = b.highColor;
    this->supp      = b.supp;

    return *this;
}

block::block(block&& b) :
    bId      (std::move(b.bId)),
    dId      (std::move(b.dId)),
    nsId     (std::move(b.nsId)),
    lowColor (std::move(b.lowColor)),
    baseColor(std::move(b.baseColor)),
    highColor(std::move(b.highColor)),
    supp     (std::move(b.supp))
{
    spdlog::trace("block::block(block&&)");}

block& block::operator=(block&& b)
{
    spdlog::trace("block::operator=(block&&)");

    this->bId       = std::move(b.bId);
    this->dId       = std::move(b.dId);
    this->nsId      = std::move(b.nsId);
    this->lowColor  = std::move(b.lowColor);
    this->baseColor = std::move(b.baseColor);
    this->highColor = std::move(b.highColor);
    this->supp      = std::move(b.supp);

    return *this;
}

block::block
(
    const          uint8_t& bi,
    const          uint8_t& di,
    const    std::  string& ni,
    const Magick::ColorRGB& lc,
    const Magick::ColorRGB& bc,
    const Magick::ColorRGB& hc,
    const             bool& s /*= false*/
) :
    bId      (bi),
    dId      (di),
    nsId     (ni),
    lowColor (lc),
    baseColor(bc),
    highColor(hc),
    supp     (s)
{
    spdlog::trace("block::block(const uint8_t& = {}, const uint8_t& = {}, const std::string& = {}, const Magick::ColorRGB& = {}, const Magick::ColorRGB& = {}, const Magick::ColorRGB& = {})", bi, di, ni, lc, bc, hc);
}

block::block
(
    const          uint8_t& bi,
    const          uint8_t& di,
    const    std::  string& ni,
    const Magick::ColorRGB& bc,
    const             bool& s /*= false*/
) :
    bId      (bi),
    dId      (di),
    nsId     (ni),
    baseColor(bc),
    supp     (s)
{
    spdlog::trace("block::block(const uint8_t& = {}, const uint8_t& = {}, const std::string& = {}, const Magick::ColorRGB& = {})", bi, di, ni, bc);
    this->baseToOthers();
}

/*virtual*/ block::~block()
    {spdlog::trace("block::~block()");}

//----------------//
// PUBLIC MEMBERS //
//----------------//

uint8_t& block::blockId()
{
    spdlog::trace("block::blockId()");
    return this->bId;
}
const uint8_t& block::blockId() const
{
    spdlog::trace("block::blockId() const");
    return this->bId;
}

uint8_t& block::dataId()
{
    spdlog::trace("block::dataId()");
    return this->dId;
}
const uint8_t& block::dataId() const
{
    spdlog::trace("block::dataId() const");
    return this->dId;
}

std::string& block::namespacedId()
{
    spdlog::trace("block::namespacedId()");
    return this->nsId;
}
const std::string& block::namespacedId() const
{
    spdlog::trace("block::namespacedId() const");
    return this->nsId;
}

Magick::ColorRGB& block::color(const block::colorType& t /*= block::colorType::base*/)
{
    spdlog::trace("block::color(const block::colorType& = {})", t);
    switch (t)
    {
        case block::colorType::low:
            return this->lowColor;
        break;

        case block::colorType::base:
            return this->baseColor;
        break;

        case block::colorType::high:
            return this->highColor;
        break;

        default:
            throw std::invalid_argument("invalid colorType value");
        break;
    }
}
const Magick::ColorRGB& block::color(const block::colorType& t /*= block::colorType::base*/) const
{
    spdlog::trace("block::color(const block::colorType& = {}) const", t);
    switch (t)
    {
        case block::colorType::low:
            return this->lowColor;
        break;

        case block::colorType::base:
            return this->baseColor;
        break;

        case block::colorType::high:
            return this->highColor;
        break;

        default:
            throw std::invalid_argument("invalid colorType value");
        break;
    }
}

block::colorType block::getColorType(const Magick::ColorRGB& c) const
{
    spdlog::trace("block::getColorType(const Magick::ColorRGB& = {})", c);

    if (c == this->lowColor)
        return block::colorType::low;
    else if (c == this->baseColor)
        return block::colorType::base;
    else if (c == this->highColor)
        return block::colorType::high;
    else
        throw std::invalid_argument("block doesn't have the given color");
}

bool& block::needsSupport()
{
    spdlog::trace("block::needsSupport()");
    return this->supp;
}
const bool& block::needsSupport() const
{
    spdlog::trace("block::needsSupport() const");
    return this->supp;
}

//-----------------//
// PRIVATE MEMBERS //
//-----------------//

void block::baseToOthers()
{
    spdlog::trace("block::baseToOthers(const Magick::ColorRGB&)");

    this->lowColor  = Magick::ColorRGB(this->baseColor.red()  *(9.0/11.0),
                                       this->baseColor.blue() *(9.0/11.0),
                                       this->baseColor.green()*(9.0/11.0));

    this->highColor = Magick::ColorRGB(this->baseColor.red()  *(51.0/44.0),
                                       this->baseColor.blue() *(51.0/44.0),
                                       this->baseColor.green()*(51.0/44.0));
}

//-------------//
// NON-MEMBERS //
//-------------//

bool operator==(const block& lhs, const block& rhs)
{
    spdlog::trace("operator==(const block& = {}, const block& = {})", lhs, rhs);

    if (   lhs.bId       == rhs.bId
        && lhs.dId       == rhs.dId
        && lhs.nsId      == rhs.nsId
        && lhs.lowColor  == rhs.lowColor
        && lhs.baseColor == rhs.baseColor
        && lhs.highColor == rhs.highColor)
        return true;
    else
        return false;
}

bool operator<(const block& lhs, const block& rhs)
{
    spdlog::trace("operator<(const block& = {}, const block& = {})", lhs, rhs);

    if (lhs.bId < rhs.bId)
        return true;
    else if (lhs.bId > rhs.bId)
        return false;
    else // lhs.bId == rhs.bId
    {
        if (lhs.dId < rhs.dId)
            return true;
        else if (lhs.dId > rhs.dId)
            return false;
        else // lhs.dId == rhs.dId
        {
            if (lhs.nsId < rhs.nsId)
                return true;
            else // lhs.nsId >= rhs.nsId
                return false;
        }
    }
}

std::ostream& operator<<(std::ostream& lhs, const block& rhs)
    {return lhs << fmt::format("{}", rhs);}

std::ostream& operator<<(std::ostream& lhs, const Magick::ColorRGB& rhs)
    {return lhs << fmt::format("{}", rhs);}
