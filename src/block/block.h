/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <iostream>
#include <string>
#include <stdint.h>

#include <Magick++.h>

// check if spdlog has internal fmt
#include <spdlog/tweakme.h>
#if !defined(SPDLOG_FMT_EXTERNAL)
    #include <spdlog/fmt/bundled/format.h>
#else
    #include <fmt/format.h>
#endif

//! Represents a Minecraft block and it's map color(s).
class block
{
    //---------//
    // FRIENDS //
    //---------//

    friend   bool operator==(const block&, const block&);
    friend   bool operator< (const block&, const block&);
    friend struct fmt::formatter<block>;

    public:

        //-------//
        // ENUMS //
        //-------//

        //! Specifies which of the three Magick::ColorRGB s stored in \ref block to use.
        /*! \see palette::methodType */
        enum class colorType : uint8_t
        {
            low,  //!< Relates to block::lowColor.
            base, //!< Relates to block::baseColor.
            high  //!< Relates to block::highColor.
        };

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! Default constructor.
        /*! The default block is a Air block (0:0 or "minecraft:air") with all three Magick::ColorRGB s set to \c #000000. */
        block();

        //! Copy constructor.
        /*! \param b \ref block to copy from. */
        block(const block&);

        //! Copy operator.
        /*! \param b \ref block to copy from.
            \returns A reference to the \ref block copied to. */
        block& operator=(const block&);

        //! Move constructor.
        /*! \param b \ref block to move from. */
        block(block&&);

        //! Move operator.
        /*! \param b \ref block to move from.
            \returns A reference to the \ref block moved to. */
        block& operator=(block&&);

        //! Constructor accepting IDs and all 3 Magick::ColorRGB s.
        /*! \param bi The value to initialize block::bId to.
            \param di The value to initialize block::dId to.
            \param ni The value to initialize block::nsId to.
            \param lc The value to initialize block::lowColor to.
            \param bc The value to initialize block::baseColor to.
            \param hc The value to initialize block::highColor to.
            \param s The value to initialize block::supp to.
            \see block::block(const uint8_t&,const uint8_t&,const std::string&,const Magick::ColorRGB&,const bool&) */
        block
        (
            const          uint8_t&,
            const          uint8_t&,
            const    std::  string&,
            const Magick::ColorRGB&,
            const Magick::ColorRGB&,
            const Magick::ColorRGB&,
            const             bool& = false
        );

        //! Constructor accepting IDs and the base Magick::ColorRGB.
        /*! block::lowColor and block::highColor are derived from \p bc using block::baseToOthers().

            \warning If the block you're trying to represent does *not* conform to the brightness ratios found in vanilla blocks, you should use block::block(const uint8_t&,const uint8_t&,const std::string&,const Magick::ColorRGB&,const Magick::ColorRGB&,const Magick::ColorRGB&,const bool&) instead.
            \param bi The value to initialize block::bId to.
            \param di The value to initialize block::dId to.
            \param ni The value to initialize block::nsId to.
            \param bc The value to initialize block::baseColor to and to derive the other Magick::ColorRGB s.
            \param s The value to initialize block::supp to. */
        block
        (
            const          uint8_t&,
            const          uint8_t&,
            const    std::  string&,
            const Magick::ColorRGB&,
            const             bool& = false
        );

        //! Destructor.
        virtual ~block();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Returns a reference to block::bId.
        /*! \see blockId() const */
              uint8_t& blockId()      ;
        //! \c const version of block::blockId().
        const uint8_t& blockId() const;

        //! Returns a reference to block::dId.
        /*! \see dataId() const */
              uint8_t& dataId()      ;
        //! \c const version of block::dataId().
        const uint8_t& dataId() const;

        //! Returns a reference to block::nsId.
        /*! \see namespacedId() const */
              std::string& namespacedId()      ;
        //! \c const version of block::namespacedId().
        const std::string& namespacedId() const;

        //! Returns a reference to one of the Magick::ColorRGB s.
        /*! \param t Which Magick::ColorRGB to return a reference to.
            \throws std::invalid_argument if \p t is an invalid value.
            \see color() const */
              Magick::ColorRGB& color(const colorType& = colorType::base)      ;
        //! \c const version of block::color().
        const Magick::ColorRGB& color(const colorType& = colorType::base) const;

        //! Returns a block::colorType that corresponds to the Magick::ColorRGB passed.
        /*! Should be used in combination with \ref palette::find(const Magick::ColorRGB&,const palette::methodType&) const .
            \throws std::invalid_argument if \p c doesn't match any Magick::ColorRGB in the \ref block.*/
        colorType getColorType(const Magick::ColorRGB&) const;

        //! Returns a reference to block::supp.
        /*! \see needsSupport() const */
              bool& needsSupport()      ;
        //! \c const version of block::needsSupport().
        const bool& needsSupport() const;

    private:

        //------//
        // DATA //
        //------//

        //! The block's numerical block ID.
        /*! Used in older versions of Minecraft, before 1.12.
            \see dId */
        uint8_t bId;

        //! The block's numerical data ID.
        /*! Used in older versions of Minecraft:
                - with block IDs before 1.12
                - with namespaced IDs for 1.12.x

            \see bId nsId */
        uint8_t dId;

        //! The block's namespaced string ID.
        /*! Used in modern versions of Minecraft:
                - with data IDs for 1.12.x
                - exclusively for 1.13.x and onward

            \see dId */
        std::string nsId;

        //! The block's lower color.
        /*! If the block north (-z) of this block is higher, this is the color this block will show on a map.
            \see color(const colorType&) color(const colorType&) const colorType baseColor highColor */
        Magick::ColorRGB lowColor;

        //! The block's base color.
        /*! If the block north (-z) of this block is of the same height, this is the color this block will show on a map.
            \see color(const colorType&) color(const colorType&) const colorType lowColor highColor */
        Magick::ColorRGB baseColor;

        //! The block's higher color.
        /*! If the block north (-z) of this block is lower, this is the color this block will show on a map.
            \see color(const colorType&) color(const colorType&) const colorType lowColor baseColor */
        Magick::ColorRGB highColor;

        //! A boolean specifing if the block needs a support block below it to show it's color.
        /*! Obviously this includes blocks that can only exist on top of another block like *Weighted Pressure Plate*, but also *Iron Bars*, which (found out through testing) also needs a block under it to make it show it's color.
            \see needsSupport() needsSupport() const */
        bool supp;

        //-----------------//
        // PRIVATE MEMBERS //
        //-----------------//

        //! Uses block::baseColor to derive block::lowColor and block::highColor using the brightness ratios found in vanilla blocks.
        /*! \see block::block(const uint8_t&,const uint8_t&,const std::string&,const Magick::ColorRGB&,const bool&) */
        void baseToOthers();
};

//-------------//
// NON-MEMBERS //
//-------------//

//! \ref block == operator.
/*! Used in std::find().
    \return \c true if all the data members in both \ref block s are equal. */
bool operator==(const block&, const block&);

//! \ref block < operator.
/*! Used in std::set<block>.
    The data values are compared in this order:
        1. block::bId
        2. block::dId
        3. block::nsId */
bool operator< (const block&, const block&);

//! \ref block std::ostream operator.
/*! Used for spdlog. Calls <tt>fmt::format("{}", rhs)</tt>.
    \param lhs The std::ostream to output to.
    \param rhs The \ref block to output.
    \returns A reference to \p lhs . */
std::ostream& operator<<(std::ostream&, const block&);

//! Magick::ColorRGB std::ostream operator.
/*! Used for spdlog. Calls <tt>fmt::format("{}", rhs)</tt>.
    \param lhs The std::ostream to output to.
    \param rhs The Magick::ColorRGB to output.
    \returns A reference to \p lhs . */
std::ostream& operator<<(std::ostream&, const Magick::ColorRGB&);

//! fmt::formatter instantiation for Magick::ColorRGB .
template <>
struct fmt::formatter<Magick::ColorRGB>
{
    constexpr auto parse(format_parse_context& ctx)
    {
        auto it = ctx.begin();
        while (it != ctx.end() && *it != '}')
            ++it;
        return it;
    }

    template <typename FormatContext>
    auto format(const Magick::ColorRGB& c, FormatContext& ctx)
    {
        return format_to
        (
            ctx.out(),
            "[#{:x} {:x} {:x}]",
            (uint8_t)(c.red()  *255),
            (uint8_t)(c.green()*255),
            (uint8_t)(c.blue() *255)
        );
    }
};

//! fmt::formatter instantiation for \ref block .
template <>
struct fmt::formatter<block>
{
    constexpr auto parse(format_parse_context& ctx)
    {
        auto it = ctx.begin();
        while (it != ctx.end() && *it != '}')
            ++it;
        return it;
    }

    template <typename FormatContext>
    auto format(const block& b, FormatContext& ctx)
    {
        return format_to
        (
            ctx.out(),
            "[{},{}, {}, {},{},{}, {}]",
            b.bId,
            b.dId,
            b.nsId,
            b.lowColor,
            b.baseColor,
            b.highColor,
            b.supp
        );
    }
};
