/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <array>
#include <memory>

#include "block.h"

template <std::size_t W, std::size_t H, std::size_t L>
class schematic;

//-------------//
// NON-MEMBERS //
//-------------//

//! \ref schematic std::ostream operator.
/*! Exports to a .schematic file using nbt++.
    \param lhs The std::ostream to export to.
    \param rhs The \ref schematic to export.
    \returns A reference to \c lhs.
    \warning Make sure \c lhs is set to binary mode, or output will be mangled on Windows systems. */
template <std::size_t W, std::size_t H, std::size_t L>
std::ostream& operator<<(std::ostream&, const schematic<W,H,L>&);

//! Represents a 3D std::array of \ref block s.
/*! Uses the same coordinate system as in-game.
    \tparam W,H,L The width (X), height (Y), and length (Z) of the \ref schematic, respectively. */
template <std::size_t W, std::size_t H, std::size_t L>
class schematic
{
    //---------//
    // FRIENDS //
    //---------//

    friend std::ostream& operator<<<W,H,L>(std::ostream&, const schematic<W,H,L>&);

    public:

        //----------//
        // TYPEDEFS //
        //----------//

        //! 3D array type used internally.
        using array_type = std::array<block, W*H*L>;

        //! Index type of \ref array_type.
        using size_type = typename array_type::size_type;

        //! \c const iterator type of \ref array_type.
        using const_iterator = typename array_type::const_iterator;

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! Default constructor.
        /*! Initializes \ref blocks to be filled with default \ref block s.
            \see block::block() */
        schematic<W,H,L>();

        //! Copy constructor.
        /*! \param s schematic to copy from. */
        schematic<W,H,L>(const schematic<W,H,L>&);

        //! Copy operator.
        /*! \param s schematic to copy from.
            \returns A reference to the schematic copied to. */
        schematic<W,H,L>& operator=(const schematic<W,H,L>&);

        //! Move constructor.
        /*! \param s schematic to move from. */
        schematic<W,H,L>(schematic<W,H,L>&&);

        //! Move operator.
        /*! \param s schematic to move from.
            \returns A reference to the schematic moved to. */
        schematic<W,H,L>& operator=(schematic<W,H,L>&&);

        //! Constructor accepting a std::array as defined in \ref array_type.
        /*! \param a The std::array to initialize \ref blocks to. */
        schematic<W,H,L>(array_type&&);

        //! Destructor.
        virtual ~schematic<W,H,L>();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Get a block at a specific position in the \ref schematic.
        /*! \param x,y,z A set of coordinates.
            \returns The \ref block at those coordinates. */
              block& at(const size_type&,
                        const size_type&,
                        const size_type&)      ;

        //! \c const version of at().
        const block& at(const size_type&,
                        const size_type&,
                        const size_type&) const;

        //! Fills an inclusive range of blocks.
        /*! \param x1,y1,z1 The first set of coordinates of the range.
            \param x2,y2,z2 The second set of coordinates of the range.
            \param b The \ref block to set the range to.
            \returns A refernce to the \ref schematic called. */
        schematic<W,H,L>& fill(const size_type&,
                               const size_type&,
                               const size_type&,
                               const size_type&,
                               const size_type&,
                               const size_type&,
                               const block&);

        //! Returns the beginning \c const iterator to the raw \ref blocks array.
        /*! Used in operator<<(std::ostream&, const schematic&). */
        const_iterator cbegin() const;

        //! Returns the off-the-end \c const iterator to the raw \ref blocks array.
        /*! Used in operator<<(std::ostream&, const schematic&). */
        const_iterator cend() const;

    private:

        //------//
        // DATA //
        //------//

        //! Contains the \ref block s in the \ref schematic.
        /*! Encased in a std::unique_ptr so the data is placed in dynamic memory rather than the stack. This is due to it's large size. */
        std::unique_ptr<array_type> blocks;

        /*
            (x,y,z)
            in game cords
        */

        //-----------------//
        // PRIVATE MEMBERS //
        //-----------------//

        //! Flattens a 3D coordinate into a 1D index.
        /*! Used internally by at().
            \param x,y,z A 3D set of coordinates.
            \returns The corresponding 1D index in \ref blocks. */
        inline const size_type flattenIndex(const size_type&,
                                            const size_type&,
                                            const size_type&) const;
};

#include "schematic_cpp.h"
