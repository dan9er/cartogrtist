/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <array>
#include <memory>

#include <Magick++.h>

#include "block.h"
#include "palette.h"
#include "schematic.h"

extern template class std::array<block,129>;
extern template class std::array<std::array<block,129>,128>;
extern template class schematic<128,2,129>;

//! Represents a mapart that uses the palette::methodType::flat method.
/*! \see mapArtStaircase */
class mapArtFlat
{
    public:

        //----------//
        // TYPEDEFS //
        //----------//

        //! 2D array type used internally.
        using array_type = std::array<std::array<block,129>,128>;

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! Default constructor.
        /*! Initializes \ref blocks to be filled with default \ref block s.
            \see block::block() */
        mapArtFlat();

        //! Copy constructor.
        /*! \param m mapArtFlat to copy from. */
        mapArtFlat(const mapArtFlat&);

        //! Copy operator.
        /*! \param m mapArtFlat to copy from.
            \returns A reference to the mapArtFlat copied to. */
        mapArtFlat& operator=(const mapArtFlat&);

        //! Move constructor.
        /*! \param m mapArtFlat to move from. */
        mapArtFlat(mapArtFlat&&);

        //! Move operator.
        /*! \param m mapArtFlat to move from.
            \returns A reference to the mapArtFlat moved to. */
        mapArtFlat& operator=(mapArtFlat&&);

        //! Constructor accepting a 2D std::array as defined in \ref array_type.
        /*! \param a The std::array to initialize \ref blocks to. */
        mapArtFlat(array_type&&);

        //! Constructor accepting an Magick::Image and \ref palette.
        /*! \param i The Magick::Image to convert to mapArtFlat.
            \param p The \ref palette to use.
            \warning This constructor assumes \p i conforms to the palette \p p, and so does \e not catch any exceptions thrown by \ref block or \ref palette. */
        mapArtFlat(const Magick::Image&, const palette&);

        //! Destructor.
        virtual ~mapArtFlat();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Generates and returns a \ref schematic.
        /*! \returns The \ref schematic representation of the mapArtFlat. */
        schematic<128,2,129> exportSchematic() const;

    private:

        //------//
        // DATA //
        //------//

        //! Contains the \ref block s in the mapArtFlat.
        /*! Encased in a std::unique_ptr so the data is placed in dynamic memory rather than the stack. This is due to it's large size.

        blocks | In-game
        --: | :--
        +x | +x (East)
        +y | -z (North)
        */
        std::unique_ptr<array_type> blocks;

        /*
            y
            .
            .
            .
            4XXXXX
            3XXXXX
            2XXXXX
            1XXXXX
            0XXXXX
             01234...x

            (x,y)
        */
};
