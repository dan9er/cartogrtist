/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include "mapArtFlat.h"

#include <array>
#include <memory>
#include <utility>

#include <Magick++.h>

#include <spdlog/spdlog.h>

#include "block.h"
#include "palette.h"
#include "schematic.h"

extern template class std::array<block,129>;
extern template class schematic<128,2,129>;

//--------------//
// CTORS & DTOR //
//--------------//

mapArtFlat::mapArtFlat() :
    blocks(new mapArtFlat::array_type())
{
    spdlog::trace("mapArtFlat::mapArtFlat()");}

mapArtFlat::mapArtFlat(const mapArtFlat& m) :
    blocks(new mapArtFlat::array_type(*(m.blocks)))
{
    spdlog::trace("mapArtFlat::mapArtFlat(const mapArtFlat&)");}

mapArtFlat& mapArtFlat::operator=(const mapArtFlat& m)
{
    spdlog::trace("mapArtFlat::operator=(const mapArtFlat&)");

    // don't do anything if trying to copy to self
    if (this->blocks != m.blocks)
        this->blocks.reset(new mapArtFlat::array_type(*(m.blocks)));

    return *this;
}

mapArtFlat::mapArtFlat(mapArtFlat&& m) :
    blocks(m.blocks.release())
{
    spdlog::trace("mapArtFlat::mapArtFlat(mapArtFlat&&)");}

mapArtFlat& mapArtFlat::operator=(mapArtFlat&& m)
{
    spdlog::trace("mapArtFlat::operator=(mapArtFlat&&)");

    // don't do anything if trying to move to self
    if (this->blocks != m.blocks)
        this->blocks.reset(m.blocks.release());

    return *this;
}

mapArtFlat::mapArtFlat(mapArtFlat::array_type&& a) :
    blocks(new mapArtFlat::array_type(std::move(a)))
{
    spdlog::trace("mapArtFlat::operator=(mapArtFlat::array_type&&)");}

mapArtFlat::mapArtFlat(const Magick::Image& i, const palette& p) :
    blocks(new mapArtFlat::array_type())
{
    spdlog::trace("mapArtFlat::mapArtFlat(Magick::Image&, palette&)");

    for (mapArtFlat::array_type::size_type x = 0; x != 128; ++x)
    {
        spdlog::debug("x: {}", x);
        std::array<block,129> colVec;

        for (std::array<block,129>::size_type y = 0; y != 128; ++y)
        {
            colVec.at(128) = block(3,0, "minecraft:dirt", Magick::ColorRGB("#000000"));

            spdlog::debug("y: {}", y);

            block b(p.find(i.pixelColor(x, y), palette::methodType::flat));
            spdlog::debug("block used: {}", b);

            colVec.at(127-y) = std::move(b);
        }

        spdlog::trace("Moving colVec into this->blocks");
        this->blocks->at(x) = std::move(colVec);
    }
}

/*virtual*/ mapArtFlat::~mapArtFlat()
    {spdlog::trace("mapArtFlat::~mapArtFlat()");}

//----------------//
// PUBLIC MEMBERS //
//----------------//

schematic<128,2,129> mapArtFlat::exportSchematic() const
{
    spdlog::trace("mapArtFlat::exportSchematic()");

    // create empty schematic
    spdlog::info("-- Initializing empty schematic");
    schematic<128,2,129> ret;

    // place the blocks
    spdlog::info("-- Placing blocks from mapart object");
    for (uint_fast8_t x = 0; x != 128; ++x)
        for (uint_fast8_t y = 0; y != 129; ++y)
        {
            spdlog::trace("x,y = ({},{})", x, y);

            const block& b = this->blocks->at(x).at(y);

            if (b.needsSupport())
                ret.at(x, 0, 128-y) = block(3,0, "minecraft:dirt", Magick::ColorRGB("#000000"));

            ret.at(x, 1, 128-y) = b;
        }

    // return schematic
    return ret;
}
