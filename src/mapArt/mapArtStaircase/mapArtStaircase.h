/*  cartogrtist
    Copyright (C) 2020  dan9er & contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#pragma once

#include <array>
#include <memory>

#include <Magick++.h>

#include "staircaseColumn.h"
#include "palette.h"
#include "schematic.h"

extern template class std::array<staircaseColumn,128>;
extern template class schematic<128,130,129>;

//! Represents a mapart that uses the palette::methodType::staircase method.
/*! \see mapArtFlat */
class mapArtStaircase
{
    public:

        //----------//
        // TYPEDEFS //
        //----------//

        //! Array type used internally.
        using array_type = std::array<staircaseColumn,128>;

        //--------------//
        // CTORS & DTOR //
        //--------------//

        //! Default constructor.
        /*! Initializes \ref columns to be filled with default \ref staircaseColumn s.
            \see staircaseColumn::staircaseColumn() */
        mapArtStaircase();

        //! Copy constructor.
        /*! \param m mapArtStaircase to copy from. */
        mapArtStaircase(const mapArtStaircase&);

        //! Copy operator.
        /*! \param m mapArtStaircase to copy from.
            \returns A reference to the mapArtStaircase copied to. */
        mapArtStaircase& operator=(const mapArtStaircase&);

        //! Move constructor.
        /*! \param m mapArtStaircase to move from. */
        mapArtStaircase(mapArtStaircase&&);

        //! Move operator.
        /*! \param m mapArtStaircase to move from.
            \returns A reference to the mapArtStaircase moved to. */
        mapArtStaircase& operator=(mapArtStaircase&&);

        //! Constructor accepting an std::array as defined in \ref array_type.
        /*! \param a The std::array to initialize \ref columns to. */
        mapArtStaircase(array_type&&);

        //! Constructor accepting an Magick::Image and \ref palette.
        /*! \param i The Magick::Image to convert to mapArtStaircase.
            \param p The \ref palette to use.
            \warning This constructor assumes \p i conforms to the palette \p p, and so does \e not catch any exceptions thrown by \ref block or \ref palette. */
        mapArtStaircase(const Magick::Image&, const palette&);

        //! Destructor.
        virtual ~mapArtStaircase();

        //----------------//
        // PUBLIC MEMBERS //
        //----------------//

        //! Generates and returns a \ref schematic.
        /*! \returns The \ref schematic representation of the mapArtStaircase. */
        schematic<128,130,129> exportSchematic();

    private:

        //------//
        // DATA //
        //------//

        //! Contains the \ref block s in the mapArtStaircase.
        /*! Encased in a std::unique_ptr so the data is placed in dynamic memory rather than the stack. This is due to it's large size.

        columns | In-game
        --: | :--
        +x | +x (East)
        +\ref staircaseColumn::blocks | -z (North)
        +\ref staircaseColumn::height | +y (Up)
        */
        std::unique_ptr<array_type> columns;

        /*
            y
            .
            .
            .
            4XXXXX
            3XXXXX
            2XXXXX
            1XXXXX
            0XXXXX
             01234...x

             y is staircaseColumn.blocks
            +z points out and is staircaseColumn.height

            (x,y,z)
        */
};
