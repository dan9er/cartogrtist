# cartogrtist
**cartographer + artist (car-TOG-art-ist)**

An image to Minecraft map art converter in C++11.

**NOTE:** At the moment, cartogrtist assumes the input image's colors conforms to the set palette.

## Features
- Command line interface
- Can read from STDIN / write to STDOUT
- Full logging
- Multiple mapart making methods
- Outputs to `*.schematic`

For planned features, see `enhancement` tagged issues on the GitLab repository.

## Releases & Changelog
Changelogs and pre-built executables can be found at https://gitlab.com/dan9er/cartogrtist/-/releases.

### Pre-built Limitations
Both pre-builts are only guaranteed to accept PNG files, this is to reduce file-size.

**Linux Pre-built:** Requires `libpng16.so` to be present on your system, but it should be installed by default on your distro.

**Windows Pre-built:** Requires the bundled `delegates.mgk` file to be in the same directory as `cartogrtist.exe` to function. This is a limitation of GraphicsMagick.

## Usage
`cartogrtist [-q] ... [-v] ... [-m <flat|staircase>] [-p <_1.12>] [--] [--version] [-h] <*.png, etc.|-> <*.schematic|->`

Where:

### `-C`,  `--colorless-log`
Use tags instead of color in logging. Useful if redirecting logger output to file.

### `-q`, `--quiet` (accepted multiple times)
Decrease the logging level.

### `-v`, `--verbose` (accepted multiple times)
Increase the logging level.

### `-m <flat|staircase>`, `--method <flat|staircase>`
In-game map-making method.

| Option | Description | Available Colors |
| --- | --- | --- |
| `flat` | Simply lays the blocks flat. | `Base Colors` |
| `staircase` (default) | Places the blocks as they were stairs, taking advantage of the fact that maps brighten/darken blocks that are higher/lower than the block north of it. | 3 * `Base Colors` |

### `-p <_1.12>`, `--palette <_1.12>`
Color palette to use.

| Option | Description | Base Colors |
| --- | --- | --- |
| `_1.12` (default) | Palette for Minecraft version 1.12. | 50 |

### `--`, `--ignore_rest`
Ignores the rest of the labeled arguments following this flag.

### `--version`
Displays version information and exits.

### `-h`, `--help`
Displays usage information and exits.

### `<-|*.png, etc.>` (required)
Filename of image to convert. The image **must** have a size of 128x128.  
If `-`, read from standard input.

### `<-|*.schematic>`
Filename for outputted schematic file.  
If `-`, write to standard output.  
If empty, defaults to `[input].schematic`.

### Logging Levels
cartogrtist logs to standard error.

| Level | Description |
| --- | --- |
| `off` | No logging whatsoever. A non-zero exit code will be the only indication that something went wrong. |
| `critical` | Logs fatal errors only. Fatal errors can't be recovered from and will force cartogrtist to exit. |
| `error` | Logs non-fatal errors and above. Errors are problems that will cause undesired output, but execution could continue. |
| `warning` | Logs warnings and above. Warnings are problems that may or may not cause undesired output or later errors. Eg. assumptions and depreciated options. |
| `info` (default) | Logs info messages and above. Info messages show the end-user what cartogrtist is doing. |
| `debug` | Logs debug messages and above. Debug messages show info that may be helpful to the end-user to self-troubleshoot problems. |
| `trace` | Logs "everything". This essentially logs every function call and more. You probably shouldn't use this unless you're asked to, since it is *VERY* spammy and only useful for debugging. |

## Building from source
cartogrtist supports the following build platforms:

- [Linux](https://gitlab.com/dan9er/cartogrtist/-/wikis/Building/Building-on-Linux)
- [Windows via MSYS2](https://gitlab.com/dan9er/cartogrtist/-/wikis/Building/Building-on-Windows-via-MSYS2)

### Direct Dependacies
- **[GraphicsMagick++](http://www.graphicsmagick.org/Magick++/index.html):** For reading and manipulating the input image
- **[libnbt++](https://github.com/ljfa-ag/libnbtplusplus):** For writing NBT data
- **[spdlog](https://github.com/gabime/spdlog):** For logging
- **[TCLAP](http://tclap.sourceforge.net/):** Provides the CLI

## Why did you make this?
- As a personal challenge to myself
- To learn how to use and link external libraries
- To learn how to use build systems (CMake, Make, Ninja, MSYS2)
- To learn how to manage a open-source project
- To improve on RedstoneHelper's Java program (which [was outdated](https://old.reddit.com/r/Minecraft/comments/2yck3f/i_made_a_tool_to_help_you_create_map_art_in/) when I started, but was modernized while I was ~~coding~~ dealing with build issues for v0.0)

## License
License information can be found in the LICENSE.txt file.
